﻿using System;
using System.Linq;
using kayak.Helper;
using NUnit.Framework;
using OpenQA.Selenium;

namespace kayak
{
    [TestFixture]
    public class UC_DesktopTests 
    {
        public UC_DesktopTests()  { }

        private IWebDriver driver;

        [SetUp]
        public void SetupTest()
        {
            driver = Constants.Driver;
            driver.Manage().Window.Maximize();
            NavigateTo.Page(Constants.baseurl, driver);
        }
        [TearDown]
        public void TeardownTest()
        {
            driver.Quit();
        }

        [Test]

        public void EndDateLessThanStartDate()
        {    
            SearchHelper.SetFrom("moscow", driver);

            SearchHelper.SetSameDate(driver);
            SearchHelper.SetStartTime("23:00", driver); 
            SearchHelper.SetEndTime("22:00", driver);

            Actions.ClickBy(By.CssSelector(UIConstants.searchButton), driver);
            AssertIs.TextContainsInSelector(By.CssSelector(UIConstants.errorrText), Constants.errorEndDateLessThanStartDate, driver);
            Actions.ClickBy(By.CssSelector(UIConstants.closeErrorButton), driver);
            WaitElement.WaitDisappearsElement(By.CssSelector(UIConstants.errorrText), driver);
        }

        [Test]
        public void EmptyFrom()
        {
            Actions.ClickBy(By.CssSelector(UIConstants.searchButton), driver);

            AssertIs.TextContainsInSelector(By.CssSelector(UIConstants.errorrText), Constants.errorEmptyFrom, driver);
            Actions.ClickBy(By.CssSelector(UIConstants.closeErrorButton), driver);
            WaitElement.WaitDisappearsElement(By.CssSelector(UIConstants.errorrText), driver);
        }

        [Test]
        public void EmptyTo()
        {
            SearchHelper.SelectOtherPlaceReturn(driver);

            SearchHelper.SetFrom("moscow", driver);
            Actions.ClickBy(By.CssSelector(UIConstants.searchButton), driver);

            AssertIs.TextContainsInSelector(By.CssSelector(UIConstants.errorrText), Constants.errorEmptyTo, driver);
            Actions.ClickBy(By.CssSelector(UIConstants.closeErrorButton), driver);
            WaitElement.WaitDisappearsElement(By.CssSelector(UIConstants.errorrText), driver);
        }
    }
}
